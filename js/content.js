'use strict';

const EMAIL_REGEXP = new RegExp("[a-zA-Z0-9.]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*", 'gm');

$(function () {

    console.log('EmailFinder INIT');

    function parse() {

        let html = $('html').html();

        let match;
        let emails = [];

        while ((match = EMAIL_REGEXP.exec(html)) !== null ) {

            let existedEmail = emails[match[0]];

            if(!existedEmail || emails[match[0]].url !== location.href) {
                emails.push({
                    email: match[0],
                    url: location.href,
                    icon: $('link[rel="icon"][type="image/x-icon"]').attr('href')
                });
            }

        }

        try {

            console.log('PARSED EMAILS:', emails);

            chrome.runtime.sendMessage({
                action: 'add',
                emails: emails
            });

        } catch (e) {
            console.error('SEND ERROR');
        }

    }

    parse();
    setInterval(parse, 10000);

});
