'use strict';

$(function () {

    let emails = $('tbody.emails');
    let emailTemplate = emails.find('tr.email.template');

    $('.export-csv').click(function (e) {
        e.preventDefault();
        getEmails(function (response) {
            if(response && response.emails) {

                let data = 'Email,URL\n';

                for(let email in response.emails) {

                    data+=response.emails[email].email+','+
                        response.emails[email].url+'\n';
                }

                let link = document.createElement('a');
                link.setAttribute('href', 'data:text/csv;charset=utf-8,'+data);
                link.setAttribute('download', 'emails.csv');
                link.click();

            }
        });
    });

    $('.export-html').click(function (e) {
        e.preventDefault();
        chrome.tabs.create({'url': chrome.extension.getURL('/html/export.html')});
    });

    function getEmails(callback) {
        chrome.runtime.sendMessage({
            action: 'list'
        }, function (response) {
            if(callback) {
                callback(response);
            }
        });
    }

    try {

        getEmails(function (response) {
            console.log(response.emails);

            if(response && response.emails) {

                for(let email in response.emails) {

                    let tr = emailTemplate.clone().removeClass('template');

                    tr.find('.email').text(response.emails[email].email);
                    tr.find('.url').append('<a>').find('a').text('URL').attr('href', response.emails[email].url)
                        .attr('target', '_blank');

                    emails.append(tr);
                }

                emails.find('.btn.btn-danger').click(function (e) {
                    e.preventDefault();

                    let btn = $(this);

                    swal({
                        title: 'Are you sure?',
                        text: "You won't be able to revert this!",
                        type: 'warning',
                        reverseButtons: true,
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, delete it!'
                    }).then((result) => {
                        if (result.value) {
                            swal(
                                'Deleted!',
                                'Your file has been deleted.',
                                'success'
                            )
                        }
                    })

                });

            }
        });

    } catch (e) {
        console.error('SEND ERROR');
    }

});