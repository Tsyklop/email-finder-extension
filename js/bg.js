'use strict';

function BG() {

    let self = this;

    let emails = localStorage.getItem('emails')?JSON.parse(localStorage.getItem('emails')):{};

    function updateEmails(email) {

        let existEmail = emails[email.email];

        if(!existEmail || existEmail.url !== email.url) {
            emails[email.email] = email;
            localStorage.setItem('emails', JSON.stringify(emails));
        }

    }

    chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
        console.log('chrome.runtime.onMessage', request, sender);

        if(request && request.action) {
            switch (request.action) {
                case 'add':

                    if(request.emails && Array.isArray(request.emails)) {

                        for(let email of request.emails) {
                            updateEmails(email);
                        }

                    }

                    break;
                case 'list':
                    sendResponse({emails: emails});
                    break;
            }
        }

    });

    return self;

}

new BG();